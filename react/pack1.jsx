/* eslint-disable no-console */
import React, { Component, useMemo, useContext } from "react";
import { injectIntl } from 'react-intl'
import { useCssHandles } from 'vtex.css-handles'
import { MyComponentProps } from './typings/global'
import { Progress } from 'vtex.styleguide'
import axios from 'axios'
import { useMutation } from 'react-apollo'
import MUTATIONS_ADD from './query/addToCart.gql'
import { useQuery } from 'react-apollo'
import { Fragment } from 'react'
import { useState } from 'react'
import { useEffect } from 'react'
import Slider from "react-slick";
import QUERY_ORDERID from './query/orderId.gql'
import QUERY_TRAT from './query/collectionT.gql'
import { useLazyQuery } from 'react-apollo'
import { OrderFormProvider, useOrderForm } from 'vtex.order-manager/OrderForm'
import { Helmet } from 'vtex.render-runtime'
import { useRuntime } from 'vtex.render-runtime';
import QUERY_SETTINGS from './query/settings.gql'
import { canUseDOM } from 'vtex.render-runtime'

//----------------------------------------------------//
import { Button, Modal } from 'vtex.styleguide'
//----------------------------------------------------//

import moment from 'moment';//https://momentjs.com/docs/
import ProductContext from './context/ProductContext';

const CSS_HANDLES = [
  'countdownheader',
  'countdownbody',
  'days',
  'hours',
  'minutes',
  'seconds',
  'daystext',
  'hourstext',
  'minutestext',
  'secondstext',
  'daysnum',
  'hoursnum',
  'minutesnum',
  'secondsnum',
  'message',
  'outlinecountdown',
  'separatorone',
  'separatortwo',
  'separatorthree',
  'title',
  'ofertaContainer',
  'ofertaHeader',
  'botonVariantMobile',
  'ofertaBody',
  'containerProducto',
  'contenedorProducto',
  'contenedorVariants',
  'containerVariants',
  'tituloVariants',
  'variants',
  'containerPrecios',
  'imagen',
  'imagen_img',
  'contenido',
  'nombre',
  'precio',
  'precioReal',
  'rowLeft',
  'verTodo',
  'imgCronometro',
  'rowRight',
  'botonComprar',
  'sliderImage',
  'fuego',
  'imgFuego',
  'separador',
  'descuento',
  'headerRowRight',

  'tituloOfertaRelampago',
  'spanTituloOfertaRelampago',
  'imagenCyberDiv',
  'imgImagenCyberDiv',

  'nowrap',
  'mr4_seguir',
  'mr4_finalizar',
  'linkseguir',
  'linkfinalizar',
  'texto_modal_kayser',
  'p_modal_kayser_enunciado',
  'p_modal-kayser_pregunta'
]
const currency = function (number) {
  return new Intl.NumberFormat('es-CL', { style: 'currency', currency: 'CLP', minimumFractionDigits: 0 }).format(number);
};

const Countdown = ({ props }) => {
  const [itemID, setItemID] = useState(0);
  const [productosFiltrados, setProductosFiltrados] = useState([]);
  const [settingsP, setSettingsP] = useState({});
  const [hide, setHide] = useState("none");
  const [terminado, setTerminado] = useState(false);
  const [width, setWidth] = useState(window.innerWidth);
  const { orderForm, setOrderForm } = useOrderForm()
  const { setProducts, productContext } = useContext(ProductContext);


  //----------------------------------------------------//
  const [isModalOpen, setIsModalOpen] = useState(false)
  //----------------------------------------------------//

  let settings = {};
  var contexto = useContext(ProductContext);
  const count = useCssHandles(CSS_HANDLES)
  var id1 = new Number(props.idCollect);
  var id1S = String(id1);

  useEffect(() => {
    setProducts(id1S);
  }, [])

  useEffect(() => {
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const handleResize = () => {
    setWidth(window.innerWidth);
  };

  if (width > 990) {
    settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      accessibility: true,
      className: count.sliderImage
    };
  } else if (width <= 990 && width > 640) {
    settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      accessibility: true,
      className: count.sliderImage
    };
  } else if (width <= 640) {
    settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 1,
      accessibility: true,
      className: count.sliderImage
    };
  }

  var fechaFin = new Date(props.endDate).getTime();
  var fechaIni = new Date(props.startDate).getTime();
  var startDate = moment(fechaIni);
  var endDateF = moment(fechaFin).format('MMMM DD YYYY, hh:mm:ss');
  var endDate = moment(fechaFin);
  var horaF = endDate.format('MMMM DD YYYY, HH:mm:ss');
  var horaA = moment().format();
  var diferencia = parseInt(startDate.diff(horaA));

  function contador() {
    var fechaFinCif = new Date(horaF).getTime();

    var fechaIniCif = new Date(endDateF).getTime();
    var x = setInterval(function () {
      var fechaActCif = new Date().getTime();
      var auxDif = fechaIniCif - fechaActCif;
      var distance = fechaFinCif - fechaActCif;
      if (auxDif <= 0) {
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        document.getElementById("hours-oferta").innerHTML = hours;

        document.getElementById("minutes-oferta").innerHTML = minutes;

        document.getElementById("seconds-oferta").innerHTML = seconds;
        if (hours == 0 && minutes == 0 && seconds == 0) {
          setTimeout(ocultarOferta, 1000);
        }
        if (distance < 0) {
          clearInterval(x);
          setHide("none");
        } else {
          setHide("block")
        }
      }

    }, 1000);
  }
  function ocultarOferta() {
    setHide("none");
  }
  function mostrarOferta() {
    if (hide === "none" && terminado === false && horaF > horaA) {
      contador();
    }

  }
  if (diferencia) {
    setTimeout(mostrarOferta, diferencia);
  }
  var collect1 = parseInt(id1S);
  //---------------------------------------------------------------------------//
  var url = "/[" + collect1 + "]?map=productClusterIds";
  const [addToCar, { data, loading, error }] = useMutation(MUTATIONS_ADD)

  const addToCart = (itemID) => {
    /*let orderId = orderForm.id;
    let cuerpo = { quantity: 1, seller: 1, id: itemID }

    let promise = addToCar({ variables: { idcart: orderId, item: cuerpo } })
    promise.then(function (response) {
      console.log(response);
      handleOpen()
    })*/
  }
  function handleOpen() {
    setIsModalOpen(true)
  }
  function handleClose() {
    setIsModalOpen(false)
  }
  function reloadSite() {
    window.location.reload()
  }
  //---------------------------------------------------------------------------//

  function changeImage(itemId, imageVariant) {
    setItemID(parseInt(itemId))

  }

  return (
    <div className={`${count.ofertaContainer}`} style={{ display: hide }}>
      <Helmet>
        <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
      </Helmet>
      <div className={`${count.ofertaHeader}`}>
        <div id="countdown-header" className={`${count.countdownheader}`}>
        </div>
        <div id="countdown-body" className={`${count.countdownbody}`}>
          <div className={`${count.rowLeft}`}>
            <h2 className={`${count.title}`}>SOLO POR POCAS HORAS</h2>
            <a className={`${count.verTodo}`} href={url} target="_blank">ver todo</a>
            <img className={`${count.imgCronometro}`} src="https://cdn-icons-png.flaticon.com/512/6874/6874028.png" />
          </div>
          <div className={`${count.rowRight}`} id="outline-countdown">
            <div className={`${count.headerRowRight}`}>
              <div id="hours-oferta" className={`${count.hours}`}></div>
              <p className={`${count.separador}`}>:</p>
              <div id="minutes-oferta" className={`${count.minutes}`}></div>
              <p className={`${count.separador}`}>:</p>
              <div id="seconds-oferta" className={`${count.seconds}`}></div>
            </div>
          </div>
          <p id="message" className={`${count.message}`}></p>
        </div>
      </div>
      <div className={`${count.tituloOfertaRelampago}`}>
        <h2 className={`${count.spanTituloOfertaRelampago}`}>OFERTA RELAMPAGO</h2>
      </div>
      <div className={`${count.ofertaBody}`}>
        {
          productContext.length > 0 ? (
            <div className={`${count.containerProducto}`}>
              <Slider {...settings}>
                {
                  productContext.map((prod, index) => (
                    <div key={index} className={`${count.contenedorProducto}`} >
                      <div className={`${count.fuego}`}><img className={`${count.imgFuego}`} src="https://cdn-icons-png.flaticon.com/512/785/785116.png" /></div>
                      <a className={`${count.imagen}`} href={prod.link}>
                        <img className={`${count.imagen_img}`} src={prod.imagen} />
                      </a>
                      <div className={`${count.contenido}`} id={"prodId" + prod.sku}>
                        <p className={`${count.nombre}`} >{prod.nombre}</p>
                        <div className={`${count.containerPrecios}`}>
                          <p className={`${count.precioReal}`} >{currency(prod.precioReal)}</p>
                          <p className={`${count.precio}`} >{currency(prod.precio)}</p>
                          <p className={`${count.descuento}`} >{"-" + prod.desc + "%"}</p>
                        </div>
                      </div>
                      <a className={`${count.botonComprar}`} href={prod.link}>Comprar</a>
                    </div>
                  ))
                }
              </Slider>
            </div>
          ) : (
            <h1>hola mundo!</h1>
          )
        }
      </div>
      <div className={`${count.imagenCyberDiv}`}>
        <img className={`${count.imgImagenCyberDiv}`} src="https://www.cyber.cl/images/logo_cyberday.svg"></img>
      </div>
      {/*-------------------------------------------------------------------------*/}
      <Modal
        isOpen={isModalOpen}
        responsiveFullScreen
        bottomBar={
          <div className={`${count.nowrap}`}>
            <span className={`${count.mr4_seguir}`}>
              <a className={`${count.linkseguir}`} onClick={reloadSite}>
                Seguir comprando
              </a>
            </span>
            <span className={`${count.mr4_finalizar}`}>
              <a className={`${count.linkfinalizar}`} href="/checkout/#/cart">
                Finalizar compra
              </a>
            </span>
          </div>
        }
        onClose={handleClose}>
        <div className={`${count.texto_modal_kayser}`}>
          <p className={`${count.p_modal_kayser_enunciado}`}>
            Tu producto ha sido agregado correctamente
          </p>
          <p className={`${count.p_modal_kayser_pregunta}`}>
            ¿Deseas seguir comprando?
          </p>
        </div>
      </Modal>
      {/*-------------------------------------------------------------------------*/}
    </div>
  );
}
export default Countdown